//
// Copyright (C) 2024 The LineageOS Project
//
// SPDX-License-Identifier: Apache-2.0
//

// Init scripts
sh_binary {
    name: "erase_batinfo.sh",
    src: "bin/erase_batinfo.sh",
    vendor: true,
}

sh_binary {
    name: "restore_batinfo.sh",
    src: "bin/restore_batinfo.sh",
    vendor: true,
}

sh_binary {
    name: "NfcFelica.sh",
    src: "bin/NfcFelica.sh",
    vendor: true,
}

sh_binary {
    name: "UTSdumpstate.sh",
    src: "bin/UTSdumpstate.sh",
    vendor: true,
}

sh_binary {
    name: "WifiAntenna.sh",
    src: "bin/WifiAntenna.sh",
    vendor: true,
}

sh_binary {
    name: "WifiMac.sh",
    src: "bin/WifiMac.sh",
    vendor: true,
}

sh_binary {
    name: "WifiSARPower.sh",
    src: "bin/WifiSARPower.sh",
    vendor: true,
}

sh_binary {
    name: "asus_mediaflag.sh",
    src: "bin/asus_mediaflag.sh",
    vendor: true,
}

sh_binary {
    name: "country.sh",
    src: "bin/country.sh",
    vendor: true,
}

sh_binary {
    name: "cscclearlog.sh",
    src: "bin/cscclearlog.sh",
    vendor: true,
}

sh_binary {
    name: "ddr_info.sh",
    src: "bin/ddr_info.sh",
    vendor: true,
}

sh_binary {
    name: "execkernelevt.sh",
    src: "bin/execkernelevt.sh",
    vendor: true,
}

sh_binary {
    name: "firmware_version.sh",
    src: "bin/firmware_version.sh",
    vendor: true,
}

sh_binary {
    name: "gf_ver.sh",
    src: "bin/gf_ver.sh",
    vendor: true,
}

sh_binary {
    name: "gsensor2_status.sh",
    src: "bin/gsensor2_status.sh",
    vendor: true,
}

sh_binary {
    name: "gyroscope2_status.sh",
    src: "bin/gyroscope2_status.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.changebinder.sh",
    src: "bin/init.asus.changebinder.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.check_asdf.sh",
    src: "bin/init.asus.check_asdf.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.check_last.sh",
    src: "bin/init.asus.check_last.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.checkdatalog.sh",
    src: "bin/init.asus.checkdatalog.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.checklogsize.sh",
    src: "bin/init.asus.checklogsize.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.kernelmessage.sh",
    src: "bin/init.asus.kernelmessage.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.piq.sh",
    src: "bin/init.asus.piq.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.ramdump.sh",
    src: "bin/init.asus.ramdump.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.thermal_conf.sh",
    src: "bin/init.asus.thermal_conf.sh",
    vendor: true,
}

sh_binary {
    name: "init.asus.zram.sh",
    src: "bin/init.asus.zram.sh",
    vendor: true,
}

sh_binary {
    name: "init.class_main.sh",
    src: "bin/init.class_main.sh",
    vendor: true,
}

sh_binary {
    name: "init.crda.sh",
    src: "bin/init.crda.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-lahaina.sh",
    src: "bin/init.kernel.post_boot-lahaina.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-shima.sh",
    src: "bin/init.kernel.post_boot-shima.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-yupik.sh",
    src: "bin/init.kernel.post_boot-yupik.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot.sh",
    src: "bin/init.kernel.post_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.mdm.sh",
    src: "bin/init.mdm.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.class_core.sh",
    src: "bin/init.qcom.class_core.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.coex.sh",
    src: "bin/init.qcom.coex.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.early_boot.sh",
    src: "bin/init.qcom.early_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.efs.sync.sh",
    src: "bin/init.qcom.efs.sync.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.post_boot.sh",
    src: "bin/init.qcom.post_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.sdio.sh",
    src: "bin/init.qcom.sdio.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.sensors.sh",
    src: "bin/init.qcom.sensors.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.sh",
    src: "bin/init.qcom.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.usb.sh",
    src: "bin/init.qcom.usb.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.chg_policy.sh",
    src: "bin/init.qti.chg_policy.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.display_boot.sh",
    src: "bin/init.qti.display_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug-lahaina.sh",
    src: "bin/init.qti.kernel.debug-lahaina.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug-shima.sh",
    src: "bin/init.qti.kernel.debug-shima.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug-yupik.sh",
    src: "bin/init.qti.kernel.debug-yupik.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug.sh",
    src: "bin/init.qti.kernel.debug.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.sh",
    src: "bin/init.qti.kernel.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.media.sh",
    src: "bin/init.qti.media.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.qcv.sh",
    src: "bin/init.qti.qcv.sh",
    vendor: true,
}

sh_binary {
    name: "mount_apd.sh",
    src: "bin/mount_apd.sh",
    vendor: true,
}

sh_binary {
    name: "parse.simcode.sh",
    src: "bin/parse.simcode.sh",
    vendor: true,
}

sh_binary {
    name: "pmodechange.sh",
    src: "bin/pmodechange.sh",
    vendor: true,
}

sh_binary {
    name: "prepare_asusdebug.sh",
    src: "bin/prepare_asusdebug.sh",
    vendor: true,
}

sh_binary {
    name: "procrankdump.sh",
    src: "bin/procrankdump.sh",
    vendor: true,
}

sh_binary {
    name: "qca6234-service.sh",
    src: "bin/qca6234-service.sh",
    vendor: true,
}

sh_binary {
    name: "savelogmtp.sh",
    src: "bin/savelogmtp.sh",
    vendor: true,
}

sh_binary {
    name: "savelogs.sh",
    src: "bin/savelogs.sh",
    vendor: true,
}

sh_binary {
    name: "savelogs_complete.sh",
    src: "bin/savelogs_complete.sh",
    vendor: true,
}

sh_binary {
    name: "saveramdump.sh",
    src: "bin/saveramdump.sh",
    vendor: true,
}

sh_binary {
    name: "sensors_factory_init.sh",
    src: "bin/sensors_factory_init.sh",
    vendor: true,
}

sh_binary {
    name: "ssr_cfg.sh",
    src: "bin/ssr_cfg.sh",
    vendor: true,
}

sh_binary {
    name: "systrace.sh",
    src: "bin/systrace.sh",
    vendor: true,
}

sh_binary {
    name: "touch_ver.sh",
    src: "bin/touch_ver.sh",
    vendor: true,
}

sh_binary {
    name: "ufs_info.sh",
    src: "bin/ufs_info.sh",
    vendor: true,
}

sh_binary {
    name: "vendor_modprobe.sh",
    src: "bin/vendor_modprobe.sh",
    vendor: true,
}

sh_binary {
    name: "widevine.sh",
    src: "bin/widevine.sh",
    vendor: true,
}

sh_binary {
    name: "wifistresstest.sh",
    src: "bin/wifistresstest.sh",
    vendor: true,
}

// Init configuration files
prebuilt_etc {
    name: "init.asus.debugtool.rc",
    src: "etc/init.asus.debugtool.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.asus.rc",
    src: "etc/init.asus.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.asus.usb.rc",
    src: "etc/init.asus.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qcom.factory.rc",
    src: "etc/init.qcom.factory.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qcom.rc",
    src: "etc/init.qcom.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qti.kernel.rc",
    src: "etc/init.qti.kernel.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qti.ufs.rc",
    src: "etc/init.qti.ufs.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.target.rc",
    src: "etc/init.target.rc",
    sub_dir: "init/hw",
    vendor: true,
}

// fstab
prebuilt_etc {
    name: "fstab.default",
    src: "etc/fstab.default",
    vendor: true,
}

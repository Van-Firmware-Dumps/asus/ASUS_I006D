#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ASUS_I006D.mk

COMMON_LUNCH_CHOICES := \
    lineage_ASUS_I006D-user \
    lineage_ASUS_I006D-userdebug \
    lineage_ASUS_I006D-eng

#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from ASUS_I006D device
$(call inherit-product, device/asus/ASUS_I006D/device.mk)

PRODUCT_DEVICE := ASUS_I006D
PRODUCT_NAME := omni_ASUS_I006D
PRODUCT_BRAND := asus
PRODUCT_MODEL := ASUS_I006D
PRODUCT_MANUFACTURER := asus

PRODUCT_GMS_CLIENTID_BASE := android-asus

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="WW_I006D-user 11 RKQ1.210303.002 33.1004.0610.60 release-keys"

BUILD_FINGERPRINT := asus/WW_I006D/ASUS_I006D:11/RKQ1.210303.002/33.1004.0610.60:user/release-keys
